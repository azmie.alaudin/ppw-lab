from django.shortcuts import render

# Enter your name here
def index(request):
	response['author'] = "Azmie Alaudin"
	html = 'lab_6/lab_6.html'
	return render(request, html, response)
